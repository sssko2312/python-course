string = "To explore different supervised learning algorithms,\
we're going to use a combination of small synthetic or artificial datasets as examples, together with some larger \
real world datasets"
dict = {word: string.index(word) for word in string.split()}
print(dict)
second = sorted(dict.values(), reverse = True)
print(second)