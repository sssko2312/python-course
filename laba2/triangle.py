def triangle_1area(base, height):
    area = (1.0 / 2) * base * height
    return area


print("Area=", triangle_1area(14, 2))


def triangle_perim(a, b=2, c=5):
    perim = (a+b+c)
    return perim


print("Perimeter=", triangle_perim(2, 3, 4))
print("Perimeter=", triangle_perim(2, 3))
print("Perimeter=", triangle_perim(2))


def triangle_area(a=10, b=11, c=12):
    s = (a + b + c) / 2
    area = (s * (s - a) * (s - b) * (s - c))
    print("First side:", a, ',second side:', b, ',third side:', c, '\nArea:', area)

triangle_area()