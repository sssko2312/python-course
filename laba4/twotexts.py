def main():
    poem = """"What's Montague? It is nor hand, nor foot,
Nor arm, nor face, nor any other part
Belonging to a man. O, be some other name.
What's in a name? That which we call a rose
By any other name would smell as sweet;"""
    newPoem = poem.replace("name", "Surname")
    with open("file1.txt", "w") as f:
        print(poem, file=f)
    with open("file2.txt", "w") as f:
        print(newPoem, file=f)
    s = input("Write the file name: ")
    if s == "file1.txt":
        print(poem)
    elif s == "file2.txt":
        print(newPoem)
    else:
        print("Error!")

if __name__ == "__main__":
    main()

#task five