list1 = [i for i in range(6, 31) if i % 2 == 0]
list2 = [i for i in range(6, 31) if i % 3 == 0]
result = list(set(list1) & set(list2))
print(result)