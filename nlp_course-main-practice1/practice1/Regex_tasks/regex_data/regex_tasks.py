import re
with open("classes_2_regexps_data_student_essays.txt", "r") as file:
    text = file.read()
mistakes = re.findall(r"(?i)\ba\b[\s\n]+[aeiou]\w+", text)
print(mistakes)       # first task

mistake = re.findall(r"(?i)(can|could|may|might|shall|shoud|must|need|have to|has to|had to)\s\bto\b", text)
print(mistake)        # second task

mista = re.findall(r"(?i)(\d[\s\n]+\bam\b|\bpm\b)", text)
print(mista)          # third task

# the fourth task isn`t clear for me, sorry

with open("classes_2_regexps_data_format-date.txt", "r") as file2:
    text_format = file2.read()
pat = re.findall(r"\d{4}.\d[0-31].\d{2}", text_format)

print(re.sub(r"([1-2][0-9][0-9][0-9])/([0-3][0-9])/([0][0-9]|[1][0-2])", "\2/\\3/\\1", text_format))     # fifth task